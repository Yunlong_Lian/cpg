#include <iostream>
#include <math.h>
//#define DEBUG

using namespace std;

class Oscillator
{
public:
	double time_interval;
	int size;    // size is the number of oscillators. (e.g., a robot with 3 joints that contains 6 oscillators, so "size" is 6.)

	double dy_r2(double a_i, double R_i, double pos, double dy1);
	double dy_r1(double dy1, double dy2);
	double update_r(double pos, double dy1);
	double dy_theta(int m, double v_i, double weights[], double thetas[], double phis[]);
	double update_theta(double theta, double dy1);
	double update_x(double r_i, double theta_i);
};
double Oscillator::dy_r2(double a_i, double R_i, double pos, double dy1)
{
	double dy2;
	dy2 = a_i * (a_i / 4 * (R_i - pos) - dy1);
	return dy2;
}

double Oscillator::dy_r1(double dy1, double dy2)
{
	double new_dy1;
	new_dy1 = dy1 + dy2 * time_interval;
	return new_dy1;
}
double Oscillator::update_r(double pos, double dy1)
{
	double p = pos + dy1 * time_interval;
	return p;
}

double Oscillator::dy_theta(int m, double v_i, double weights[], double thetas[], double phis[])
{  // m is the index of theta, so its range should be [0, size-1].
	double sum;
	int k;
	double dy;
	double pi = acos(-1.0);
	double theta1, theta2, theta3;

	if (m - 1 < 0)
		theta1 = 0;
	else
		theta1 = weights[m - 1] * sin(thetas[m - 1] - thetas[m] + phis[m - 1]);

	if (m + 1 >= size)
		theta2 = 0;
	else
		theta2 = weights[m + 1] * sin(thetas[m + 1] - thetas[m] - phis[m + 1]);

	if (m < size / 2)    // left
	{
		k = m + (size / 2) + 1;
		theta3 = weights[k] * sin(thetas[k] - thetas[m] - phis[k]);
	}
	else                 // right
	{
		k = m - (size / 2) + 1;
		theta3 = weights[k] * sin(thetas[k] - thetas[m] + phis[k]);
	}


	sum = theta1 + theta2 + theta3;
	dy = 2 * pi * v_i + sum;
	// cout << "dy_theta:  " << dy << endl;
	// cout << "size: " << size << endl;
	return dy;
}
double Oscillator::update_theta(double theta, double dy1)
{
	double new_theta;
	new_theta = theta + dy1 * this->time_interval;
#ifdef DEBUG
	cout << "NEW theta:  " << new_theta << " = " << theta << " + " << dy1 << " * "<< this->time_interval << endl;
#endif
	return new_theta;
}
double Oscillator::update_x(double r_i, double theta_i)
{
	double xi;
	xi = r_i * (1 + cos(theta_i));
	return xi;
}

class Cpg : Oscillator
{
private:
	double timestep;
	int cpg_size;  // the number of oscillators

	double a[20];
	double w[20];
	double r_dy1[20];
	double x[20];

	void set_timestep(double t);
	void set_a(double a);
	void set_w(double w);

	double update_r_i(int i, double r[], double R[]);
	double update_theta_i(int i, double v, double thetas[], double phi[]);
	double update_x_i(int i, double r_i, double theta_i);

public:
	Cpg(int s, double a, double w, double t);

	double update_angle(int i);
	void update_cpg(double r[], double theta[], double v[], double phi[], double R[], double timestep);
};

Cpg::Cpg(int s, double a, double w, double t)
{
	double num = 0;
	this->cpg_size = s;
	this->size = s;

	this->timestep = t;
	this->time_interval = t;

	this->set_a(a);
	this->set_w(w);

	for (int i = 0; i < s; i++)
	{
		Cpg::r_dy1[i] = 0;
		Cpg::x[i] = 0;
	}

#ifdef DEBUG
	cout << "initial parameters x[0] is " << x[0] << endl;
	cout << "initial parameters r_dy1[0] is " << r_dy1[0] << endl;

	cout << "initial parameters cpg_size/size is " << cpg_size<<" "<<size << endl;
	cout << "initial parameters timestep/time_interval is " << timestep << " " << time_interval << endl;
#endif
}

void Cpg::set_a(double a_i)
{
	for (int i = 0; i < cpg_size; i++)
	{
		Cpg::a[i] = a_i;
	}

#ifdef DEBUG
	cout << "a[0] is :" << a[0] <<". Input a is "<< a_i<< endl;
#endif
}

void Cpg::set_w(double w)
{
	for (int i = 0; i < cpg_size; i++)
	{
		Cpg::w[i] = w;
	}
#ifdef DEBUG
	cout << "weights are : " << this->w[0] <<".  Input weights are "<<w<< endl;
#endif
}

void Cpg::set_timestep(double t)
{
	this->time_interval = t;
	Cpg::timestep = t;
}

double Cpg::update_r_i(int i, double r[], double R[])
{
	double dy1, dy2, r_i;

	dy1 = this->r_dy1[i];
	dy2 = this->dy_r2(this->a[i], R[i], r[i], dy1);

	this->r_dy1[i] = this->dy_r1(dy1, dy2);

	r_i = this->update_r(r[i], this->r_dy1[i]);

	return r_i;
}

double Cpg::update_theta_i(int i, double v, double thetas[], double phi[])
{
	double theta_dy, theta_i;

	theta_dy = this->dy_theta(i, v, this->w, thetas, phi);
	theta_i = this->update_theta(thetas[i], theta_dy);

	return theta_i;
}

double Cpg::update_x_i(int i, double r_i, double theta_i)
{
	double x;
	x = this->update_x(r_i, theta_i);
	//this->x[i] = x;
	return x;
}

double Cpg::update_angle(int i)
{
	// The range of i is [0, cpg_size/2] because i represent the index of joint, not oscillator.
	double angle;
	angle = x[i] - x[i + cpg_size/2];
	return angle;
}

void Cpg::update_cpg(double r[], double theta[], double v[], double phi[], double R[], double t = -1)
{/*
	r[] and theta[] are the state variables of all oscillator, those are one dimensional arrays.
	v[], phi[] and R[] are the control parameters, those are also one dimensional arrays, which corrsponding to each oscillator.
	however, each element in v[] and phi[] is the same,
	but it is still programmed as an array in case of developing complex motor skills in the future.

	Note: before first using update_cpg, please set cpg_size, a[] and w[] firstly through using set_size(), set_a() and set_w() separately.
 */
 // first step, set the time step in this update.
	if(t != -1)
		this->set_timestep(timestep);
	// second step, update state variables

	for (int i = 0; i < cpg_size; i++)
	{
		// update all r (amplitude) of each oscillator
		r[i] = update_r_i(i, r, R);
		// update all theta (phase) of each oscillator
		theta[i] = update_theta_i(i, v[i], theta, phi);
		// update all x (output signal) of each oscillator
		this->x[i] = update_x_i(i, r[i], theta[i]);
		
#ifdef DEBUG
		cout << "r_dy1[" << i << "]: " << this->r_dy1[i] << "    ";
		cout << "r[" << i << "]: " << r[i] << "    ";
		cout << "theta[" << i << "]: " << theta[i] << "    ";
		cout << "x[" << i << "]: " << x[i]<<endl;
#endif
	}
#ifdef DEBUG
	cout << "---------------CPG update results---------------" << endl;
#endif
	// cpg is updated.
	// Next is to update each joint angle outside of this function by calling update_angle().
}

double sin_control(double phase, double frequency, double amplitude, double offset, double t)  // t is the currenty time not time step
{
	double output;
	output = amplitude * sin(frequency * t + phase) + offset;
	return output;
}