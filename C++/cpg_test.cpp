#include <iostream>
#include <iomanip>
#include "cpg.h"
using namespace std;

int main()
{
	int size = 14; // size must be an even number because each joint has two oscillators, maximun number is 20.
	double time_step = 0.001; // time step shuold be the current time - last time, suppose we get 0.001.
	//define the paramters
	double r[14], theta[14], v[14], phi[14], R[14];
	double angles[14]; // output joint degrees
	double a = 100;
	double weight = 4;

	for (int i = 0; i < 14; i++)
	{
		r[i] = 0.5;
		theta[i] = 1;
		v[i] = 1;
		phi[i] = 1;
		R[i] = 1;
	}

	// 1. set the parameters
	Cpg mycpg(size, a, weight, time_step);  // initialize the CPG with a few fixed parameters.
	// 2. update cpg once
	 	// The first situation that if we can get the state variables from the robot:
		//	 Before updating, we should obtain the essential parameters firstly.
		//	   state variables: r(amplitude) and theta(phase) from robot, type: array
		//	   control parameters v(speed/frequency), type: array
		//						  phi(phase),   type: array
		//						  R(amplitude):  type: array
		//								Ri = AR, for the right side oscillators, i = 0,1, ... ,size/2-1, 
		//								Ri = AL, for the right side oscillators, i = size/2, ... , size-1.

	//	The second situation that we cannot get the state variables from the robot
	//  Just use the update_cpg() function, it can update state variables automatically.

	mycpg.update_cpg(r, theta, v, phi, R, time_step); // "time_step" is the default argument that the initialised value.
	// mycpg.update_cpg(r, theta, v, phi, R);
	// 3. Get each joint angle
	//	    update_angle() only output one joint angle degree,
	//		so we can only update the only output joint that we want.

	for (int i = 0; i < 7; i++)
	{
		angles[i] = mycpg.update_angle(i);    // i is the index of a joint.
		// cout << angles[i] << endl;
	}
	// Finally, we can output the disired joint angle to each motor.


	// Update cpg 2000 times

	// double angles_trajectory[7][2000];

	for (int j = 0; j < 2000; j++)
	{
		// mycpg.update_cpg(r, theta, v, phi, R);     // time_step doesn't need to setup each iteration, so the initial time_step will be used.
		mycpg.update_cpg(r, theta, v, phi, R, time_step);
		cout<<j << " r[0]: " << r[0]<< "    " << " r[1]: " << r[1] << "    " << " r[2]: " << r[2] << "    " << " r[3]: " << r[3] << "    ";
		cout << " r[4]: " << r[4] << "    " << " r[5]: " << r[5] << "    " << " r[6]: " << r[6] << "    " << endl;
		for (int i = 0; i < 7; i++)
		{
			angles[i] = mycpg.update_angle(i);
			// angles_trajectory[i][j] = angles[i];
			cout <<"  "<<setprecision(4)<<i << "-" << angles[i];
		}
		//cout <<"joint angle 0: "<< angles_trajectory[0][j] << endl;
		cout << endl;
	}

	// sin control
	double sin_angles[7];
	double phase = 0, frequency = 1, amplitude = 1, offset = 0, time = 0; // origianl sin() function
	for (int i = 0; i < 7; i++)
	{
		sin_angles[i] = sin_control(phase, frequency, amplitude, offset, time);
	}

	return 0;
}