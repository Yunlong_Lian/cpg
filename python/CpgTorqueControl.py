# ------------------------------------------
# The University of York
# The Department of Electronic Engineering
# Robotics and Autonomous Systems Lab
# Author: Yunlong Lian, PhD student
# ------------------------------------------
import pychrono.core as chrono
import math as m
import numpy as np
import matplotlib.pyplot as  plt
class cpg(object):
    def __init__(self,alpha, beta, mu, omega_stance, omega_swing, b, gait = 'pace'):
        if gait == 'trot':
            self.cm = [[0, -1, -1, 1], [-1, 0, 1, -1],  # coupling_matrix: trot gait
                       [-1, 1, 0, -1], [1, -1, -1, 0]]
            self.omega_stance = 2*np.pi
            self.omega_swing = np.pi
            self.cells_value = [[1, 0], [-1, 0], [-1, 0], [1, 0]]
        elif gait == 'walk':
            self.cm = [[0, -1, 1, -1], [-1, 0, -1, 1],
                       [-1, 1, 0, -1], [1, -1, -1, 0]]
            self.omega_stance = 4 * np.pi
            self.omega_swing = np.pi
            self.cells_value = [[-0.5, 0], [0.5, 0], [0.5, 0], [-0.5, 0]]
            # print("walk gait")
        elif gait == 'pace':
            self.cm = [[0, -1, 1, -1], [-1, 0, -1, 1],
                       [1, -1, 0, -1], [-1, 1, -1, 0]]
            self.omega_stance = 2 * np.pi
            self.omega_swing = np.pi
            self.cells_value = [[0.5, 0], [-0.5, 0], [0.5, 0], [-0.5, 0]]
        elif gait == 'bound':
            self.cm = [[0, 1, -1, -1], [1, 0, -1, -1],
                       [-1, -1, 0, 1], [-1, -1, 1, 0]]
            self.omega_stance = np.pi
            self.omega_swing = 2*np.pi
            self.cells_value = [[-0.5, 0], [-0.5, 0], [0.5, 0], [0.5, 0]]
        else:
            raise ValueError("Error, please input a correct gait : trot or walk")
        self.alpha = alpha
        self.beta = beta
        self.mu = mu
        self.b = b
        self.pos = [1,1]

    def hopf(self,x, y, steps = 0.001):
        self.r_2 = x ** 2 + y ** 2
        #print(self.b*y)
        omega1 = self.omega_stance / (m.exp(-self.b * y) + 1)
        omega2 = self.omega_swing / (m.exp(self.b * y) + 1)
        # print(m.exp(-self.b * y))
        self.omega = omega1 + omega2

        dx = self.alpha * (self.mu - self.r_2) * x - self.omega * y
        dy = self.beta * (self.mu - self.r_2) * y + self.omega * x

        return [x + dx*steps, y + dy*steps]

    def coupling(self, cells, steps = 0.001):
        # all_legs are defined as following:
        # all_legs = [[leg0_x,leg0_y], [leg1_x,leg1_y],
                    # [leg2_x,leg2_y], [leg3_x,leg3_y]]
        x = []
        y = []
        new_cells = []
        for i in range(len(cells)):
            x.append(cells[i][0])
            y.append(cells[i][1])
        # print("x: ", x)
        # print("y: ", y)
        if len(cells) != len(self.cm):
            raise ValueError("Error, the number of legs should be equal to the column number of coupling matrics")

        for i in range(len(cells)):
            for j in range(len(self.cm[0])):
                cells[i][1] += self.cm[i][j]*cells[j][1]*steps        # yi
            new_cells.append(cells[i])

        return new_cells

    def coupled_hopf(self, number):
        cell = []
        for i in range(len(self.cells_value)):
            new_pos = self.hopf(self.cells_value[i][0],self.cells_value[i][1])
            cell.append(new_pos)

        new_cell = self.coupling(cell)

        for i in range(len(self.cells_value)):
            self.cells_value[i][0] = new_cell[i][0]
            self.cells_value[i][1] = new_cell[i][1]
        return self.cells_value[number][0], self.cells_value[number][1]

    def get_trajectory(self, pos, data_number = 5000):
        x = []
        y = []
        px = pos[0]
        py = pos[1]
        for i in range(data_number):
            position = self.hopf(px, py)
            px = position[0]
            py = position[1]
            x.append(position[0])
            y.append(position[1])
        return x,y

    def get_all_trajectory(self, data_number = 5000):
        cells = []
        cell = []
        l1_x = self.cells_value[0][0]
        l1_y = self.cells_value[0][1]
        l2_x = self.cells_value[1][0]
        l2_y = self.cells_value[1][1]
        l3_x = self.cells_value[2][0]
        l3_y = self.cells_value[2][1]
        l4_x = self.cells_value[3][0]
        l4_y = self.cells_value[3][1]

        cell.append([l1_x,l1_y])
        cell.append([l2_x,l2_y])
        cell.append([l3_x,l3_y])
        cell.append([l4_x,l4_y])
        cells.append(cell)

        for i in range(data_number-1):
            l1_pos = self.hopf(l1_x, l1_y)
            l2_pos = self.hopf(l2_x, l2_y)
            l3_pos = self.hopf(l3_x, l3_y)
            l4_pos = self.hopf(l4_x, l4_y)
            cell[0] = l1_pos
            cell[1] = l2_pos
            cell[2] = l3_pos
            cell[3] = l4_pos
            new_cell = self.coupling(cell)
            # print(new_cell)
            l1_x = new_cell[0][0]
            l1_y = new_cell[0][1]
            l2_x = new_cell[1][0]
            l2_y = new_cell[1][1]
            l3_x = new_cell[2][0]
            l3_y = new_cell[2][1]
            l4_x = new_cell[3][0]
            l4_y = new_cell[3][1]

            cells.append(new_cell)
        return cells

class ForceCpgControl(chrono.ChFunction_SetpointCallback, cpg):
    def __init__(self, motor, joint_number, gait = 'bound'):
        chrono.ChFunction_Setpoint.__init__(self)
        chrono.ChFunction_SetpointCallback.__init__(self)
        cpg.__init__(self,100, 100, 0.09, np.pi*2, np.pi, 50, gait)
        self.PI = 3.14159
        self.f = 0  # force
        self.kp = 25
        self.ki = 1
        self.kd = 0.022
        self.last_time = 0
        self.last_error = 0
        self.motor = motor
        self.times = 0
        self.c = cpg
        self.x, self.y = self.c.hopf(self,1,1)
        self.offset = 0
        self.jn = joint_number

    def SetpointCallback(self, x):
        time = x
        error_di = 0
        self.x = self.cells_value[self.jn][0]
        self.y = self.cells_value[self.jn][1]

        if time > self.last_time:
            dt = time - self.last_time

            error = self.x+self.offset - self.motor.GetMotorRotPeriodic()
            error_dt = (error - self.last_error) / dt
            error_di += error * dt

            self.f = self.kp * (error + self.ki * error_di + self.kd * error_dt)
            self.last_time = time
            self.last_error = error

            self.c.coupled_hopf(self, self.jn)

        return self.f+1

    # def SetpointCallback(self, x):
    #     time = x
    #     error_di = 0
    #     new_x = self.x
    #     new_y = self.y
    #
    #     if time > self.last_time:
    #         dt = time - self.last_time
    #
    #         error = self.x+self.offset - self.motor.GetMotorRotPeriodic()
    #         error_dt = (error - self.last_error) / dt
    #         error_di += error * dt
    #
    #         self.f = self.kp * (error + self.ki * error_di + self.kd * error_dt)
    #         self.last_time = time
    #         self.last_error = error
    #         self.x, self.y = self.c.hopf(self,new_x, new_y)
    #
    #     return self.f

if __name__ == '__main__':
    pass
    ccpg = cpg(100, 100, 1, np.pi*2, np.pi, 100, 'bound')
    cells_trace = ccpg.get_all_trajectory(10000)
    x1 = []
    x2 = []
    x3 = []
    x4 = []
    t = np.arange(0, 10, 0.001)
    for j in range(len(cells_trace)):
        x1.append(cells_trace[j][0][0])
        x2.append(cells_trace[j][1][0])
        x3.append(cells_trace[j][2][0])
        x4.append(cells_trace[j][3][0])

    fig0 = plt.figure()
    plt.subplot(4,1,1)
    plt.plot(t, x1, color='red', label='x1')
    plt.axis([0, 10, -1.1, 1.1])
    plt.subplot(4, 1, 2)
    plt.plot(t, x2, color='orange', label='x2')
    plt.axis([0, 10, -1.1, 1.1])
    plt.subplot(4, 1, 3)
    plt.plot(t, x3, color='blue', label='x3')
    plt.axis([0, 10, -1.1, 1.1])
    plt.subplot(4, 1, 4)
    plt.plot(t, x4, color='green', label='x4')
    plt.axis([0, 10, -1.1, 1.1])
    # plt.legend(loc="upper left")
    plt.tight_layout()
    # plt.savefig('trot gait.png')
    plt.show()