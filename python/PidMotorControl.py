# ------------------------------------------
# The University of York
# The Department of Electronic Engineering
# Robotics and Autonomous Systems Lab
# Author: Yunlong Lian, PhD student
# ------------------------------------------
import pychrono.core as chrono
from copy import deepcopy

class MyTorqueCurve(chrono.ChFunction) :
  def __init__(self, e2, r2, x2, n_s, mot) :
        super().__init__()
        self.E2 = e2
        self.R2 = r2
        self.X2 = x2
        self.ns = n_s
        self.mymotor = mot

  def Clone(self) :
        return deepcopy(self)

  def Get_y(self, x) :
        # The three-phase torque(speed) model
        w = self.mymotor.GetMotorRot_dt()
        s = (self.ns - w) / self.ns  # slip
        T = (3.0 / 2 * chrono.CH_C_PI * self.ns) * (s * self.E2 * self.E2 * self.R2) / (self.R2 * self.R2 + pow(s * self.X2, 2))  # electric torque curve
        T -= w * 5  # simulate also a viscous brake
        # T = 49
        return T

class ForceAngleControl(chrono.ChFunction_SetpointCallback):
    def __init__(self, angle, motor):
        chrono.ChFunction_Setpoint.__init__(self)
        chrono.ChFunction_SetpointCallback.__init__(self)
        self.PI = 3.14159
        self.angle = angle
        self.rad = self.PI / 180 * angle
        self.f = 0  # force
        self.kp = 25
        self.ki = 1
        self.kd = 0.022
        self.last_time = 0
        self.last_error = 0
        self.motor = motor
        self.times = 0

    def SetpointCallback(self, x):
        time = x
        error_di = 0

        if time > self.last_time:
            self.times += 1
            dt = time - self.last_time
            error = self.rad - self.motor.GetMotorRotPeriodic()

            error_dt = (error - self.last_error) / dt
            error_di += error * dt

            self.f = self.kp * (error + self.ki * error_di + self.kd * error_dt)
            self.last_time = time
            self.last_error = error
            # self.f = self.kp * (error + self.kd * error_dt)
            # print("angles: ",self.motor.GetMotorRotPeriodic()," ",self.rad)
            # print("force: ", self.f, "angle: ", self.motor.GetMotorRotPeriodic())
            # if abs(error) < 0.01:
            #     print("error: ", self.times, ' ', self.last_error)
            #     pass
        return self.f

if __name__ == "__main__":
    pass