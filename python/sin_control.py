import numpy as np
import matplotlib.pyplot as plt

phase = 0
amplitude = 1
frequency = 1
offset = 0

plt.figure()
x = np.linspace(0, 50, 2500)
y = np.sin(frequency*x+phase) * amplitude + offset

plt.subplot(411)
plt.plot(x, y)
plt.xlabel("Time")
plt.ylabel("Angle")
plt.ylim(-2, 2)

phase = 0.5 * np.pi
amplitude = 1
frequency = 1
offset = 0
x1 = np.linspace(0, 50, 2500)
y1 = np.sin(frequency*x+phase) * amplitude + offset

plt.subplot(412)
plt.plot(x1, y1)
plt.xlabel("Time")
plt.ylabel("Angle")
plt.ylim(-2, 2)

phase = 1 * np.pi
amplitude = 1
frequency = 2
offset = 0
x2 = np.linspace(0, 50, 2500)
y2 = np.sin(frequency*x+phase) * amplitude + offset

plt.subplot(413)
plt.plot(x2, y2)
plt.xlabel("Time")
plt.ylabel("Angle")
plt.ylim(-2, 2)

phase = 1 * np.pi
amplitude = 1
frequency = 2
offset = 0.5
x3 = np.linspace(0, 50, 2500)
y3 = np.sin(frequency*x+phase) * amplitude + offset

plt.subplot(414)
plt.plot(x3, y3)
plt.xlabel("Time")
plt.ylabel("Angle")
plt.ylim(-2, 2)
plt.tight_layout()
plt.show()

